from django.shortcuts import render


# Create your views here.
def main(request):
    return render(request, 'about_me.html')


def education(request):
    return render(request, 'education.html')


def job(request):
    return render(request, 'job.html')